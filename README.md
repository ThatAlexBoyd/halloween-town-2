# README #

Halloween Town 2 is the continuation of the VR Unity game created for a company Halloween party. The game was designed and built to be played with the Occulus VR for kids ages 6-10.

In Halloween Town 2 you ride along rails through a spooky environment and try to find various objects for the Pumpking!

### Contributions ###

* Hedgehog Team (Environment Assets)