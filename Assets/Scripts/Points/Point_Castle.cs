﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Point_Castle : Point {

    public GameObject door_left;
    public GameObject door_right;


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            Found_Key();
        }
    }

    public override void Init()
    {
        base.Init();
    }

    //key has been found
    public void Found_Key()
    {
        AudioManager.Instance.Play_KeyFound();
        StartCoroutine(DelayDoor());
    }

    public IEnumerator DelayDoor()
    {
        yield return new WaitForSeconds(2f);
        OpenDoor();
        yield return new WaitForSeconds(1f);
        OnFinished();
    }

    private void OpenDoor()
    {
        door_left.transform.DORotate(new Vector3(0, -80, 0), 1f, RotateMode.LocalAxisAdd);
        door_right.transform.DORotate(new Vector3(0, 80, 0), 1f, RotateMode.LocalAxisAdd);
    }

}
