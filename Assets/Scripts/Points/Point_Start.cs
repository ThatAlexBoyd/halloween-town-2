﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Point_Start : Point {

    public FindObject candy;
    public CanvasGroup cg;

    public void Awake()
    {
        Init();
        cg.alpha = 1;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            FoundCandy();
        }
    }

    public override void Init()
    {
        base.Init();
        DOTween.To(Callback, 1, 0, 4f);
        AudioManager.Instance.Play_Opening();
        StartCoroutine(PostAudio());

    }

    public IEnumerator PostAudio()
    {
        yield return new WaitForSeconds(15f);
        candy.GetComponent<Collider>().enabled = true;
    }

    public void FoundCandy()
    {
        pc.ResumeTween();
    }

    public void Callback(float f)
    {
        cg.alpha = f;
    }
}
