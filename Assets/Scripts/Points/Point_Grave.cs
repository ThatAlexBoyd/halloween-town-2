﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Point_Grave : Point {

    bool found_milk = false;
    bool found_waffle = false;
    bool found_burger = false;

    public GameObject gate_left;
    public GameObject gate_right;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            FoundMilk();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            FoundBurger();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            FoundWaffle();
        }
    }


    public override void Init()
    {
        base.Init();
    }


    public void FoundMilk()
    {
        //milk audio
        AudioManager.Instance.Play_FoodMilk();
        found_milk = true;
        FoundCheck();
    }

    public void FoundBurger()
    {
        //burger audio
        AudioManager.Instance.Play_FoodHamburger();
        found_burger = true;
        FoundCheck();
    }

    public void FoundWaffle()
    {
        //waffle audio
        AudioManager.Instance.Play_FoodWaffle();
        found_waffle = true;
        FoundCheck();
    }

    public void FoundCheck()
    {
        if(found_burger && found_milk && found_waffle)
        {
            StartCoroutine(DelayGate());
        }
    }

    public IEnumerator DelayGate()
    {
        yield return new WaitForSeconds(4);
        AudioManager.Instance.Play_FoodEnd();
        yield return new WaitForSeconds(4);
        OnFinished();
        OpenGate();
    }

    private void OpenGate()
    {
        gate_left.transform.DORotate(new Vector3(0, 0, 80), 1f, RotateMode.LocalAxisAdd);
        gate_right.transform.DOLocalRotate(new Vector3(0, 0, -80), 1f, RotateMode.LocalAxisAdd);
    }
}
