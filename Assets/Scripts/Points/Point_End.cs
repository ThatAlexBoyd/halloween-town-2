﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class Point_End : Point {

    public CanvasGroup cg;
    public Animator logo;
    public Animator kingHead;
    public override void Init()
    {
       base.Init();
        StartCoroutine(DelayEnd());
    }

    IEnumerator DelayEnd()
    {
        yield return new WaitForSeconds(1f);
        AudioManager.Instance.Play_CandyOpen();
        kingHead.SetTrigger("end");
        yield return new WaitForSeconds(7f);
        AudioManager.Instance.Play_CandyNumber(GameObject.FindObjectOfType<GameManager>().CandyCount()-1);
        yield return new WaitForSeconds(5);
        DOTween.To(Callback, 0, 1, 3f);
        yield return new WaitForSeconds(3);
        logo.SetTrigger("title");

    }

    public void Callback(float f)
    {
        cg.alpha = f;
    }

    public void AnimateHead()
    {

        kingHead.SetTrigger("looking");
    }
}
