﻿using UnityEngine;
using System.Collections;

public class AudioManager : UnitySingleton<AudioManager> {

    public AudioSource source;
    [Header("Opening")]
    public AudioClip clip_opening;

    [Header("Castle")]
    public AudioClip clip_key_opening;
    public AudioClip clip_key_found;

    public AudioClip clip_castle;

    [Header("Grave Food")]
    public AudioClip clip_food_opening;
    public AudioClip clip_food_hamburger;
    public AudioClip clip_food_milk;
    public AudioClip clip_food_waffle;
    public AudioClip clip_food_end;

    [Header("Grave")]
    public AudioClip clip_grave_opening;
    public AudioClip clip_grave_candy_start;
    public AudioClip[] clip_grave_candy_number;
    public AudioClip clip_grave_end;

    [Header("SFX")]
    public AudioClip clip_pickup;



    public void Play_Opening()
    {
        Play(clip_opening);
    }

    public void Play_KeyOpening()
    {
        Play(clip_key_opening);
    }

    public void Play_KeyFound()
    {
        Play(clip_key_found);
    }

    public void Play_Castle()
    {
        Play(clip_castle);
    }

    public void Play_FoodOpening()
    {
        Play(clip_food_opening);
    }

    public void Play_FoodHamburger()
    {
        Play(clip_food_hamburger);
    }

    public void Play_FoodMilk()
    {
        Play(clip_food_milk);
    }

    public void Play_FoodWaffle()
    {
        Play(clip_food_waffle);
    }

    public void Play_FoodEnd()
    {
        Play(clip_food_end);
    }

    public void Play_GraveOpen()
    {
        Play(clip_grave_opening);
    }

    public void Play_CandyOpen ()
    {
        Play(clip_grave_candy_start);
    }

    public void Play_CandyNumber(int num)
    {
        Play(clip_grave_candy_number[num]);
    }

    public void Play_GraveEnd()
    {
        Play(clip_grave_end);
    }

    public void Play_Pickup()
    {
        Play(clip_pickup, false);
    }


    private void Play(AudioClip clip, bool cutoff = true)
    {
        if (cutoff) { source.Stop(); }
        source.PlayOneShot(clip);
    }
}
