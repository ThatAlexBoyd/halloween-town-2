﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PathController : MonoBehaviour {

    public DOTweenPath playerPath;
    public ObjectController objCont;
    public Point_End pe;

	void Update ()
    {
        playerPath.tween.OnWaypointChange(CheckPoint);

        if(Input.GetKeyDown(KeyCode.X))
        {
            Time.timeScale = 1.5f;
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Time.timeScale = 1;
        }
    }

    /// <summary>
    /// Points are unfortunately hard coded - beware
    /// </summary>
    /// <param name="index"></param>
    public void CheckPoint(int index)
    {
        switch(index)
        {
            case 2: //after start
                objCont.Enable_Candy();
                break;
            case 4:
                AudioManager.Instance.Play_KeyOpening();
                Debug.Log("KEY START");
                break;
            case 5: //outside castle wall
                playerPath.tween.Pause();
                objCont.Enable_Castle();
                break;
            case 8:
                AudioManager.Instance.Play_Castle();
                break;
            case 12: //gate before going into cemetery
                AudioManager.Instance.Play_FoodOpening();
                playerPath.tween.Pause();
                objCont.Enable_Grave();
                break;
            case 13:
                GameObject.FindObjectOfType<Point_End>().AnimateHead();
                AudioManager.Instance.Play_GraveOpen();
                break;
            case 14: //at skelton king
                playerPath.tween.Pause();
                pe.Init();
                break;
            default:
                //Debug.Log("Not Used Point: " + index);
                break;
        } 

    }
    
    public void ResumeTween()
    {
        playerPath.tween.Play();
    }
}
