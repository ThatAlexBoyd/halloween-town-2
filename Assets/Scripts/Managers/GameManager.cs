﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private int candyCount = 0;

	// Use this for initialization
	void Start () {
        candyCount = 0;
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void Found_Candy()
    {
        candyCount++;
    }

    public int CandyCount()
    {
        return candyCount;
    }

}
